Attribute VB_Name = "view"
Public Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long

Public Declare Function ShowWindow Lib "user32" (ByVal hwnd As Long, ByVal nCmdShow As Long) As Long

Public Const SW_HIDE = 0
Public Const SW_SHOW = 5

Public Const GWL_EXTYLE = (-20)
Public Const WS_EX_TOOLWINDOW = &H80&

Public Sub setShowInTaksbar(Visible As Boolean, hwnd As Long)

Dim L As Long
L = ShowWindow(hwnd, SW_HIDE)
DoEvents

L = SetWindowLong(hwnd, GWL_EXTYLE, IIf(Visible, WS_EX_TOOLWINDOW, WS_EX_TOOLWINDOW))

DoEvents
L = ShowWindow(hwnd, SW_SHOW)
End Sub




