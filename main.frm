VERSION 5.00
Begin VB.Form Main 
   BackColor       =   &H00FFFFFF&
   Caption         =   "Sistem Antrian RS Prof Dr Tabrani"
   ClientHeight    =   11115
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13155
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   FillColor       =   &H00C0FFC0&
   ForeColor       =   &H0080FF80&
   Icon            =   "main.frx":0000
   LinkTopic       =   "Form1"
   Picture         =   "main.frx":058A
   ScaleHeight     =   11115
   ScaleWidth      =   13155
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrMarquee 
      Interval        =   100
      Left            =   7920
      Top             =   120
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "PROSES PELAYANAN"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Left            =   120
      TabIndex        =   1
      Top             =   6720
      Width           =   12855
      Begin VB.Label Loket4_lbl 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   39.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   9960
         TabIndex        =   14
         Top             =   1080
         Width           =   2535
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         Caption         =   "COUNTER 4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9960
         TabIndex        =   13
         Top             =   480
         Width           =   2535
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         Caption         =   "COUNTER 3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   6840
         TabIndex        =   12
         Top             =   480
         Width           =   2535
      End
      Begin VB.Label label1 
         Alignment       =   2  'Center
         BackColor       =   &H000080FF&
         Caption         =   "COUNTER 1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   360
         TabIndex        =   11
         Top             =   480
         Width           =   2535
      End
      Begin VB.Label labelC2 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         Caption         =   "COUNTER 2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   3600
         TabIndex        =   10
         Top             =   480
         Width           =   2535
      End
      Begin VB.Label Loket3_lbl 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   39.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   6840
         TabIndex        =   7
         Top             =   1080
         Width           =   2535
      End
      Begin VB.Label Loket2_lbl 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   39.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   3600
         TabIndex        =   6
         Top             =   1080
         Width           =   2535
      End
      Begin VB.Label Loket1_lbl 
         Alignment       =   2  'Center
         BackColor       =   &H000080FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   39.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   360
         TabIndex        =   5
         Top             =   1080
         Width           =   2535
      End
   End
   Begin VB.Timer timer_auto_refresh 
      Interval        =   1000
      Left            =   6480
      Top             =   120
   End
   Begin VB.Timer timer_autorefresh_pelayanan 
      Interval        =   1000
      Left            =   6960
      Top             =   120
   End
   Begin VB.Timer tmrclock 
      Interval        =   100
      Left            =   7440
      Top             =   120
   End
   Begin VB.Image Image2 
      Height          =   1665
      Left            =   120
      Picture         =   "main.frx":09CE
      Top             =   9480
      Width           =   20040
   End
   Begin VB.Label nomor_antrian_txt_bpjs 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFF80&
      BackStyle       =   0  'Transparent
      Caption         =   "B - 21"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   72
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   1815
      Left            =   960
      TabIndex        =   4
      Top             =   3120
      Width           =   4455
   End
   Begin VB.Label lblTextNomor 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "ANTRIAN BPJS"
      BeginProperty Font 
         Name            =   "Arial Rounded MT Bold"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   840
      TabIndex        =   2
      Top             =   1920
      Width           =   4455
   End
   Begin VB.Label Label8 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   22
      Top             =   1800
      Width           =   6255
   End
   Begin VB.Label loket_txt_bpjs 
      BackStyle       =   0  'Transparent
      Caption         =   "4"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   35.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   975
      Left            =   4320
      TabIndex        =   16
      Top             =   4920
      Width           =   615
   End
   Begin VB.Label lblTextLoket 
      BackStyle       =   0  'Transparent
      Caption         =   "COUNTER"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   30
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   975
      Left            =   1560
      TabIndex        =   3
      Top             =   5040
      Width           =   2655
   End
   Begin VB.Label lbAntrian 
      BackColor       =   &H00C0FFC0&
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   48
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4335
      Left            =   120
      TabIndex        =   0
      Tag             =   "AutoSizer:W"
      Top             =   1800
      Width           =   6255
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "ANTRIAN UMUM / PERUSAHAAN"
      BeginProperty Font 
         Name            =   "Arial Rounded MT Bold"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7440
      TabIndex        =   18
      Top             =   1920
      Width           =   5055
   End
   Begin VB.Label Label4 
      BackColor       =   &H00FFFFFF&
      Height          =   615
      Index           =   0
      Left            =   6600
      TabIndex        =   15
      Top             =   1800
      Width           =   6255
   End
   Begin VB.Label loket_txt_umum 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "1"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   35.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   10800
      TabIndex        =   21
      Top             =   4800
      Width           =   615
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "COUNTER"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   30
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   975
      Left            =   8160
      TabIndex        =   20
      Top             =   4920
      Width           =   2655
   End
   Begin VB.Label nomor_antrian_txt_umum 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "U - 200"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   60
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   7560
      TabIndex        =   19
      Top             =   3240
      Width           =   4455
   End
   Begin VB.Label Label5 
      BackColor       =   &H000080FF&
      Height          =   4215
      Left            =   6600
      TabIndex        =   17
      Top             =   1800
      Width           =   6255
   End
   Begin VB.Label lblClock 
      Alignment       =   2  'Center
      BackColor       =   &H80000007&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   735
      Left            =   10440
      TabIndex        =   9
      Top             =   0
      Width           =   2895
   End
   Begin VB.Label lblDay 
      Alignment       =   2  'Center
      BackColor       =   &H80000007&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   735
      Left            =   7200
      TabIndex        =   8
      Top             =   0
      Width           =   3615
   End
   Begin VB.Image Image1 
      Height          =   1245
      Left            =   0
      Picture         =   "main.frx":E12E
      Top             =   0
      Width           =   20475
   End
End
Attribute VB_Name = "Main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim MyForm As FRMSIZE
Dim DesignX As Integer
Dim DesignY As Integer

Dim WithEvents RSList As Recordset
Attribute RSList.VB_VarHelpID = -1
Dim urut As String
Dim Sounds(16) As String
Dim status_sound As String
Dim loket_tujuan As String
Dim id As Integer
Dim nomor_antrian As String
Dim jenis_antrian As String
Dim x As Integer
Dim gambar As Integer
Dim qtyImage As String
Dim readFile(5) As String

Dim bulan_hijriah(1 To 12) As String

'================ SOUND TO CALL QUEUE ================
Sub panggil_L1(x)
    Dim mystr() As String
    Dim terbilang As String
    Dim loket As String
    Dim status_antrian As String
    Dim real_number As String
    Dim tes As String
    
    tes = x
    If tes = "B" Then
        real_number = Mid(nomor_antrian_txt_bpjs.Caption, 4, Len(nomor_antrian_txt_bpjs.Caption) - 3)
        terbilang = Trim((Bilang(CInt(real_number))))
        loket = Trim(Bilang(Right(loket_txt_bpjs.Caption, 1)))
        status_antrian = Trim(Left(nomor_antrian_txt_bpjs.Caption, 1))
   
        mystr = Split(terbilang, " ")
        terbilang = Trim((Bilang(CInt(real_number))))
    Else
        real_number = Mid(nomor_antrian_txt_umum.Caption, 4, Len(nomor_antrian_txt_umum.Caption) - 3)
        terbilang = Trim((Bilang(CInt(real_number))))
        loket = Trim(Bilang(Right(loket_txt_umum.Caption, 1)))
        status_antrian = Trim(Left(nomor_antrian_txt_umum.Caption, 1))
   
        mystr = Split(terbilang, " ")
    End If
        
    Call sndPlaySound(App.Path & "\Sounds\nomor-urut.wav", SND_NOSTOP)
    Call sndPlaySound(App.Path & "\Sounds\" & status_antrian & ".wav", SND_NOSTOP)
    
        For I = LBound(mystr) To UBound(mystr)
            Call sndPlaySound(App.Path & "\Sounds\" & mystr(I) & ".wav", SND_NOSTOP)
        Next
        Call sndPlaySound(App.Path & "\Sounds\loket.wav", SND_NOSTOP)
        Call sndPlaySound(App.Path & "\Sounds\" & loket & ".wav", SND_NOSTOP)
End Sub


Private Sub Form_Load()
bulan_hijriah(1) = "Muharram"
bulan_hijriah(2) = "Safar"
bulan_hijriah(3) = "Rabiul Awal"
bulan_hijriah(4) = "Rabiul Akhir"
bulan_hijriah(5) = "Jumadil Awal"
bulan_hijriah(6) = "Jumadil Akhir"
bulan_hijriah(7) = "Rajab"
bulan_hijriah(8) = "Sya'ban"
bulan_hijriah(9) = "Ramadhan"
bulan_hijriah(10) = "Syawal"
bulan_hijriah(11) = "Dzulka'idah"
bulan_hijriah(12) = "Dzulhijjah"


If App.PrevInstance Then ActivatePrevInstance

'================== SETTING SCREEN RESOLUTION ================
Dim tmp As String
Dim TmpArr() As String
tmp = GetSystemMetrics(SM_CXSCREEN) & "x" & GetSystemMetrics(SM_CYSCREEN)
TmpArr = Split(tmp, "x")

    Dim ScaleFactorX As Single, ScaleFactorY As Single  ' Scaling factors
    ' Size of Form in Pixels at design resolution
    DesignX = TmpArr(0)
    DesignY = TmpArr(1)
    RePosForm = True   ' Flag for positioning Form
    DoResize = False   ' Flag for Resize Event
    ' Set up the screen values
    Xtwips = Screen.TwipsPerPixelX
    Ytwips = Screen.TwipsPerPixelY
    Ypixels = Screen.Height / Ytwips ' Y Pixel Resolution
    Xpixels = Screen.Width / Xtwips  ' X Pixel Resolution
    
    ' Determine scaling factors
    ScaleFactorX = (Xpixels / DesignX)
    ScaleFactorY = (Ypixels / DesignY)
    ScaleMode = 1  ' twips
    'Exit Sub  ' uncomment to see how Form1 looks without resizing
    Resize_For_Resolution ScaleFactorX, ScaleFactorY, Me
    MyForm.Height = Me.Height ' Remember the current size
    MyForm.Width = Me.Width
          
 '================== END SETTING SCREEN RESOLUTION ================



    'Show the last queue in table
Call ActConn
Set rstake = New Recordset

rstake.Open "select top 1 *  from list order by id DESC", strConn
If rstake.EOF Then
'jika data baru ada
Else
        jenis_antrian = "B"
        nomor_antrian_txt_bpjs.Caption = jenis_antrian & " - " & rstake(2)
        loket_txt_bpjs.Caption = rstake(1)
End If
rstake.Close

Set rstake2 = New Recordset
rstake2.Open "SELECT TOP 1 * FROM list WHERE jenis = 'UMUM' ORDER BY id DESC", strConn
If rstake2.EOF Then
'jika data baru ada
Else
        jenis_antrian = "U"
        nomor_antrian_txt_umum.Caption = jenis_antrian & " - " & rstake2(2)
        loket_txt_umum.Caption = rstake2(1)
End If


'tanggal
lblDay.Caption = Format(Now, "Long Date")

Call antrian_porses_loket1
Call antrian_porses_loket2
Call antrian_porses_loket3
Call antrian_porses_loket4
Call ActConn

strHijr = ConvertDateString(Date, 0, 1, "dd/mm/yyyy")

strHijrDay = Left(strHijr, 2)
If strHijrDay = 0 Then strHijrDay = 1

'lblHijr = strHijrDay & " " & bulan_hijriah(Mid(strHijr, 4, 2)) & " " & Right(strHijr, 4)

'play wmp
'wmp.URL = App.Path & "\video\my_videos.mp4"
End Sub

Private Sub Form_Resize()
Dim ScaleFactorX As Single, ScaleFactorY As Single

If Not DoResize Then  ' To avoid infinite loop
    DoResize = True
    Exit Sub
End If

RePosForm = False
ScaleFactorX = Me.Width / MyForm.Width   ' How much change?
ScaleFactorY = Me.Height / MyForm.Height
Resize_For_Resolution ScaleFactorX, ScaleFactorY, Me
MyForm.Height = Me.Height ' Remember the current size
MyForm.Width = Me.Width

Call setShowInTaksbar(False, Me.hwnd)
End Sub

'=========== Auto Reload Queue ===============
Private Sub timer_auto_refresh_Timer()

Call ActConn

Set RSList = New Recordset
RSList.Open "Select TOP 1 * FROM list WHERE status='ON' AND sound='1'", strConn
    
'jika data baru tidak ada
If RSList.EOF Then

'jika data baru ada
Else
    nomor_antrian = RSList(2)
    jenis_antrian = RSList(7)
    status_sound = RSList(4)
    loket_tujuan = RSList(1)
    id = RSList(0)
    
    If jenis_antrian = "BPJS" Then
        jenis_antrian = "B"
        nomor_antrian_txt_bpjs.Caption = jenis_antrian & " - " & nomor_antrian
        loket_txt_bpjs.Caption = loket_tujuan
        Call panggil_L1(jenis_antrian)
    Else
        jenis_antrian = "U"
        nomor_antrian_txt_umum.Caption = jenis_antrian & " - " & nomor_antrian
        loket_txt_umum.Caption = loket_tujuan
        Call panggil_L1(jenis_antrian)
    End If
 
    If status_sound <> 0 Then
        'update status
        SQLEdit = "UPDATE LIST SET sound = '0' WHERE id='" & id & "'"
        dbconn.Execute SQLEdit
    Else
        'Do Nothing
    End If
End If
End Sub

' ============ Auto Reload Pelayanan ===============
Private Sub timer_autorefresh_pelayanan_Timer()
    Call antrian_porses_loket1
    Call antrian_porses_loket2
    Call antrian_porses_loket3
    Call antrian_porses_loket4
End Sub

Sub antrian_porses_loket1()
Call ActConn
Set RSget = New Recordset
RSget.Open "SELECT TOP 1(no_urut) FROM list WHERE status='OFF' AND actions='Proses' AND comp='1' AND jenis='UMUM' order by id DESC", strConn

If RSget.EOF Then
    Loket1_lbl.Caption = ""
Else
    Loket1_lbl.Caption = "U-" & RSget(0)
End If

End Sub

Sub antrian_porses_loket2()
Call ActConn
Set RSget = New Recordset
RSget.Open "SELECT TOP 1(no_urut) FROM list WHERE status='OFF' AND actions='Proses' AND comp='2' AND jenis='BPJS' order by id DESC", strConn

If RSget.EOF Then
    Loket2_lbl.Caption = ""
Else
    Loket2_lbl.Caption = "B-" & RSget(0)
End If

End Sub

Sub antrian_porses_loket3()
Call ActConn
Set RSList = New Recordset
RSList.Open "SELECT TOP 1(no_urut) FROM list WHERE status='OFF' AND actions='Proses' AND comp='3' AND jenis='BPJS' order by id DESC", strConn

If RSList.EOF Then
    Loket3_lbl.Caption = ""
Else
    Loket3_lbl.Caption = "B-" & RSList(0)
End If

End Sub

Sub antrian_porses_loket4()
Call ActConn
Set RSList = New Recordset
RSList.Open "SELECT TOP 1(no_urut) FROM list WHERE status='OFF' AND actions='Proses' AND comp='4'AND jenis='BPJS' order by id DESC", strConn

If RSList.EOF Then
    Loket4_lbl.Caption = ""
Else
    Loket4_lbl.Caption = "B-" & RSList(0)
End If

End Sub


Private Sub tmrclock_Timer()
lblClock.Caption = Time & " WIB"
End Sub

Function ConvertDateString( _
    ByRef StringIn As String, _
    ByRef OldCalendar As Integer, _
    ByVal NewCalendar As Integer, _
    ByRef NewFormat As String) As String

    Dim SavedCal As Integer
    Dim d As Date
    Dim s As String
    
    '// Save VBA Calendar setting to restore when finished
    SavedCal = Calendar
    
    '// Convert date to new calendar and format
    Calendar = OldCalendar      ' Change to StringIn calendar
    d = CDate(StringIn)       ' Convert from String to Date
    Calendar = NewCalendar      ' Change to calendar of new string
    s = CStr(d)          ' Convert to short format String
    ConvertDateString = Format(s, NewFormat)
    
    '// Restore VBA Calendar setting
    Calendar = SavedCal
End Function

Private Function CheckPath(strPath As String) As Boolean
    If Dir$(strPath) <> "" Then
        CheckPath = True
    Else
        CheckPath = False
    End If
End Function


' =============== MARQUEE TIMER ===========
'Private Sub tmrMarquee_Timer()
'Dim str As String
 '   str = (lblMarquee.Caption)
   ' str = Mid(str, 2, Len(str)) + Left(str, 1)
    'lblMarquee.Caption = str
'End Sub

Private Sub wmp_OpenStateChange(ByVal NewState As Long)
 wmp.settings.mute = True
wmp.settings.setMode "loop", True
End Sub
