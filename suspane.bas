Attribute VB_Name = "suspane"
Option Explicit

Declare Function OpenIcon Lib "user32" (ByVal hwnd As Long) As Long
Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Declare Function GetWindow Lib "user32" (ByVal hwnd As Long, ByVal wCmd As Long) As Long
Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long

Public Const GW_HWNDPREV = 3

Sub ActivatePrevInstance()

    Dim AppTitle As String
    Dim PrevHndl As Long
    Dim result As Long

    AppTitle = App.title
    App.title = "unwanted instance"
    
    If PrevHndl = 0 Then
        PrevHndl = FindWindow("ThunderRT6Main", AppTitle)
        If PrevHndl <> 0 Then
                PrevHndl = GetWindow(PrevHndl, GW_HWNDPREV)
                result = OpenIcon(PrevHndl)
                result = SetForegroundWindow(PrevHndl)
                End
        End If
    End If
    
End Sub
