VERSION 5.00
Object = "{90F3D7B3-92E7-44BA-B444-6A8E2A3BC375}#1.0#0"; "actskin4.ocx"
Begin VB.Form Main 
   BackColor       =   &H0080FF80&
   Caption         =   "Sistem Antrian | Developed By : Jeri Wandana, S.Kom"
   ClientHeight    =   11115
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   13155
   FillColor       =   &H00C0FFC0&
   ForeColor       =   &H0080FF80&
   LinkTopic       =   "Form1"
   ScaleHeight     =   11115
   ScaleWidth      =   13155
   StartUpPosition =   2  'CenterScreen
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   5880
      OleObjectBlob   =   "tes.frx":0000
      Top             =   240
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "AvantGarde Md BT"
         Size            =   21,75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   5520
      TabIndex        =   10
      Text            =   "LOKET 2"
      Top             =   7200
      Width           =   2415
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000D&
      Caption         =   "PROSES PELAYANAN"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080FF80&
      Height          =   3255
      Left            =   240
      TabIndex        =   4
      Top             =   6480
      Width           =   12735
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "AvantGarde Md BT"
            Size            =   21,75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   9360
         TabIndex        =   12
         Text            =   "LOKET 3"
         Top             =   720
         Width           =   2415
      End
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "AvantGarde Md BT"
            Size            =   21,75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   960
         TabIndex        =   8
         Text            =   "LOKET 1"
         Top             =   720
         Width           =   2415
      End
      Begin VB.Label Loket3_lbl 
         Alignment       =   2  'Center
         Caption         =   "24"
         BeginProperty Font 
            Name            =   "AvantGarde Bk BT"
            Size            =   39,75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   9360
         TabIndex        =   13
         Top             =   1680
         Width           =   2415
      End
      Begin VB.Label Loket2_lbl 
         Alignment       =   2  'Center
         Caption         =   "25"
         BeginProperty Font 
            Name            =   "AvantGarde Bk BT"
            Size            =   39,75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   5280
         TabIndex        =   11
         Top             =   1680
         Width           =   2415
      End
      Begin VB.Label Loket1_lbl 
         Alignment       =   2  'Center
         Caption         =   "26"
         BeginProperty Font 
            Name            =   "AvantGarde Bk BT"
            Size            =   39,75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   960
         TabIndex        =   9
         Top             =   1680
         Width           =   2415
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000B&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   5280
      Left            =   240
      ScaleHeight     =   2589.425
      ScaleMode       =   0  'User
      ScaleWidth      =   7815
      TabIndex        =   0
      Top             =   1080
      Width           =   7815
   End
   Begin VB.Timer timer_auto_refresh 
      Interval        =   1000
      Left            =   0
      Top             =   960
   End
   Begin VB.Timer timer_autorefresh_pelayanan 
      Interval        =   1000
      Left            =   600
      Top             =   960
   End
   Begin VB.Timer tmrclock 
      Interval        =   100
      Left            =   1800
      Top             =   960
   End
   Begin VB.Timer tmrMarquee 
      Interval        =   50
      Left            =   1200
      Top             =   960
   End
   Begin VB.Label lbltextAntrian 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "ANTRIAN"
      BeginProperty Font 
         Name            =   "Arial Rounded MT Bold"
         Size            =   26,25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   8280
      TabIndex        =   18
      Top             =   2160
      Width           =   4575
   End
   Begin VB.Label lblMarquee 
      BackStyle       =   0  'Transparent
      Caption         =   "  safdaf"
      BeginProperty Font 
         Name            =   "Lucida Calligraphy"
         Size            =   26,25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   0
      TabIndex        =   17
      Top             =   10080
      Width           =   13095
   End
   Begin VB.Label lblClock 
      Alignment       =   2  'Center
      BackColor       =   &H80000007&
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "AvantGarde Md BT"
         Size            =   21,75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   735
      Left            =   8160
      TabIndex        =   16
      Top             =   720
      Width           =   4815
   End
   Begin VB.Label lblDay 
      Alignment       =   2  'Center
      BackColor       =   &H80000007&
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "AvantGarde Md BT"
         Size            =   21,75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   735
      Left            =   8160
      TabIndex        =   15
      Top             =   0
      Width           =   4815
   End
   Begin VB.Shape footershape 
      BackColor       =   &H00FFFFFF&
      BorderColor     =   &H0000FF00&
      FillColor       =   &H0000FF00&
      FillStyle       =   0  'Solid
      Height          =   1215
      Left            =   -120
      Top             =   9960
      Width           =   13320
   End
   Begin VB.Label loket_txt 
      BackStyle       =   0  'Transparent
      Caption         =   "2"
      BeginProperty Font 
         Name            =   "AvantGarde Md BT"
         Size            =   27,75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   11520
      TabIndex        =   14
      Top             =   5400
      Width           =   375
   End
   Begin VB.Label nomor_antrian_txt 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "1"
      BeginProperty Font 
         Name            =   "Arial Rounded MT Bold"
         Size            =   72
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   8280
      TabIndex        =   7
      Top             =   3000
      Width           =   4575
   End
   Begin VB.Label lblTextLoket 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "LOKET "
      BeginProperty Font 
         Name            =   "Arial Rounded MT Bold"
         Size            =   27,75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   9000
      TabIndex        =   6
      Top             =   5400
      Width           =   2775
   End
   Begin VB.Label lblTextNomor 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "NOMOR"
      BeginProperty Font 
         Name            =   "Arial Rounded MT Bold"
         Size            =   26,25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   8280
      TabIndex        =   5
      Top             =   1560
      Width           =   4575
   End
   Begin VB.Label lbAntrian 
      BackColor       =   &H00FFFF00&
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   48
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4935
      Left            =   8160
      TabIndex        =   3
      Top             =   1440
      Width           =   4815
   End
   Begin VB.Label moto 
      BackStyle       =   0  'Transparent
      Caption         =   "kepuasan anda harapan kami"
      BeginProperty Font 
         Name            =   "Poor Richard"
         Size            =   14,25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      TabIndex        =   2
      Top             =   480
      Width           =   3255
   End
   Begin VB.Image Image1 
      Height          =   750
      Left            =   0
      Picture         =   "tes.frx":0234
      Top             =   0
      Width           =   1125
   End
   Begin VB.Label title 
      BackStyle       =   0  'Transparent
      Caption         =   "Rumah Sakit Prof. Dr. Tabrani"
      BeginProperty Font 
         Name            =   "Haettenschweiler"
         Size            =   21,75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   615
      Left            =   720
      TabIndex        =   1
      Top             =   0
      Width           =   4215
   End
   Begin VB.Shape header_shape 
      BorderColor     =   &H0000FF00&
      FillColor       =   &H0000FF00&
      FillStyle       =   0  'Solid
      Height          =   975
      Left            =   -3720
      Top             =   0
      Width           =   16920
   End
End
Attribute VB_Name = "Main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim MyForm As FRMSIZE
Dim DesignX As Integer
Dim DesignY As Integer

Dim WithEvents RSList As Recordset
Attribute RSList.VB_VarHelpID = -1
Dim urut As String
Dim Sounds(16) As String
Dim status_sound As String
Dim loket_tujuan As String
Dim id As Integer
Dim nomor_antrian As String
Dim x As Integer

'================ SOUND TO CALL QUEUE ================
Sub panggil_L1()
    Dim mystr() As String
    Dim terbilang As String
    Dim loket As String
    
    terbilang = Trim(Bilang(nomor_antrian_txt.Caption))
    loket = Trim(Bilang(Right(loket_txt.Caption, 1)))
    
    mystr = Split(terbilang, " ")
    
        Call sndPlaySound(App.Path & "\Sounds\nomor-urut.wav", SND_NOSTOP)
        For i = LBound(mystr) To UBound(mystr)
            Call sndPlaySound(App.Path & "\Sounds\" & mystr(i) & ".wav", SND_NOSTOP)
        Next
        Call sndPlaySound(App.Path & "\Sounds\loket.wav", SND_NOSTOP)
        Call sndPlaySound(App.Path & "\Sounds\" & loket & ".wav", SND_NOSTOP)
End Sub




Private Sub Form_Load()

If App.PrevInstance Then ActivatePrevInstance

' ================ LOAD SKIN ==================
Me.Skin1.LoadSkin App.Path & "\Green.skn"
Me.Skin1.ApplySkin Me.hwnd

'================== SETTING SCREEN RESOLUTION ================
Dim Tmp As String
Dim TmpArr() As String
Tmp = GetSystemMetrics(SM_CXSCREEN) & "x" & GetSystemMetrics(SM_CYSCREEN)
TmpArr = Split(Tmp, "x")

    Dim ScaleFactorX As Single, ScaleFactorY As Single  ' Scaling factors
    ' Size of Form in Pixels at design resolution
    DesignX = TmpArr(0)
    DesignY = TmpArr(1)
    RePosForm = True   ' Flag for positioning Form
    DoResize = False   ' Flag for Resize Event
    ' Set up the screen values
    Xtwips = Screen.TwipsPerPixelX
    Ytwips = Screen.TwipsPerPixelY
    Ypixels = Screen.Height / Ytwips ' Y Pixel Resolution
    Xpixels = Screen.Width / Xtwips  ' X Pixel Resolution
    
    ' Determine scaling factors
    ScaleFactorX = (Xpixels / DesignX)
    ScaleFactorY = (Ypixels / DesignY)
    ScaleMode = 1  ' twips
    'Exit Sub  ' uncomment to see how Form1 looks without resizing
    Resize_For_Resolution ScaleFactorX, ScaleFactorY, Me
    MyForm.Height = Me.Height ' Remember the current size
    MyForm.Width = Me.Width
          
 '================== END SETTING SCREEN RESOLUTION ================

' ================== MARQUEE TEXT =================================
lblMarquee.Caption = "Terima Kasih Atas Kepercayaan Anda Kepada Kami" & Space(30)

'tanggal
lblDay.Caption = Format(Now, "Long Date")
lblMarquee.Alignment = 1
StrCap = lblMarquee.Caption

Call antrian_porses_loket1
Call antrian_porses_loket2
Call antrian_porses_loket3
Call ActConn


    
End Sub

Private Sub Form_Resize()
Dim ScaleFactorX As Single, ScaleFactorY As Single

If Not DoResize Then  ' To avoid infinite loop
    DoResize = True
    Exit Sub
End If

RePosForm = False
ScaleFactorX = Me.Width / MyForm.Width   ' How much change?
ScaleFactorY = Me.Height / MyForm.Height
Resize_For_Resolution ScaleFactorX, ScaleFactorY, Me
MyForm.Height = Me.Height ' Remember the current size
MyForm.Width = Me.Width
          
End Sub

'=========== Auto Reload Queue ===============
Private Sub timer_auto_refresh_Timer()

Call ActConn

Set RSList = New Recordset
RSList.Open "Select TOP 1 * FROM list WHERE status='ON' AND sound='1'", strConn
    
'jika data baru tidak ada
If RSList.EOF Then

'jika data baru ada
Else
    nomor_antrian = RSList(2)
    status_sound = RSList(4)
    loket_tujuan = RSList(1)
    id = RSList(0)
    
    nomor_antrian_txt.Caption = nomor_antrian
    loket_txt.Caption = loket_tujuan
    
    If status_sound <> 0 Then
        Call panggil_L1
        
        'update status
        SQLEdit = "UPDATE LIST SET sound = '0' WHERE id='" & id & "'"
        dbconn.Execute SQLEdit
    Else
        'Do Nothing
    End If
End If
End Sub

' ============ Auto Reload Pelayanan ===============
Private Sub timer_autorefresh_pelayanan_Timer()
    Call antrian_porses_loket1
    Call antrian_porses_loket2
    Call antrian_porses_loket3
End Sub


Sub antrian_porses_loket1()
Call ActConn
Set RSget = New Recordset
RSget.Open "SELECT TOP 1(no_urut) FROM list WHERE status='OFF' AND actions='Proses' AND comp='1' order by id DESC", strConn

If RSget.EOF Then
    Loket1_lbl.Caption = ""
Else
    Loket1_lbl.Caption = RSget(0)
End If

End Sub

Sub antrian_porses_loket2()
Call ActConn
Set RSList = New Recordset
RSList.Open "SELECT TOP 1(no_urut) FROM list WHERE status='OFF' AND actions='Proses' AND comp='2' order by id DESC", strConn

'loket2 = RSList(0)

If Not RSList.EOF Then
    Loket2_lbl.Caption = RSList(0)
End If

End Sub

Sub antrian_porses_loket3()
Call ActConn
Set RSList = New Recordset
RSList.Open "SELECT TOP 1(no_urut) FROM list WHERE status='OFF' AND actions='Proses' AND comp='3' order by id DESC", strConn

'loket3 = RSList(0)

If Not RSList.EOF Then
    Loket3_lbl.Caption = RSList(0)
End If

End Sub


Private Sub tmrclock_Timer()
lblClock.Caption = Time & " WIB"
End Sub

' =============== MARQUEE TIMER ===========
Private Sub tmrMarquee_Timer()
Dim str As String
    str = lblMarquee.Caption
    str = Mid$(str, 2, Len(str)) + Left(str, 1)
    lblMarquee.Caption = str
End Sub

