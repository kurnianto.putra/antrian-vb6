VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form frmLogin 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "PLEASE LOGIN"
   ClientHeight    =   1680
   ClientLeft      =   2835
   ClientTop       =   3480
   ClientWidth     =   3825
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   992.601
   ScaleMode       =   0  'User
   ScaleWidth      =   3591.468
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ACTIVESKINLibCtl.Skin Skn 
      Left            =   1800
      OleObjectBlob   =   "frmLogin.frx":0000
      Top             =   2040
   End
   Begin VB.TextBox txtUserName 
      Enabled         =   0   'False
      Height          =   345
      Left            =   1290
      TabIndex        =   0
      Text            =   "Informasi 1"
      Top             =   240
      Width           =   2325
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   390
      Left            =   1080
      TabIndex        =   2
      Top             =   1140
      Width           =   1140
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   390
      Left            =   2400
      TabIndex        =   3
      Top             =   1140
      Width           =   1140
   End
   Begin VB.TextBox txtPassword 
      Height          =   345
      IMEMode         =   3  'DISABLE
      Left            =   1290
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   645
      Width           =   2325
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public LoginSucceeded As Boolean
Dim comp() As String

Private Sub cmdCancel_Click()
    'set the global var to false
    'to denote a failed login
    LoginSucceeded = False
    End
End Sub

Private Sub cmdOK_Click()
    'check for correct password
    If txtPassword = "antrian" Then
        'place code to here to pass the
        'success to the calling sub
        'setting a global var is the easiest
        LoginSucceeded = True
        
        comp = Split(txtUserName, " ")
        antrianPanel.txtLoket.Text = comp(1)
        Me.Hide
        antrianPanel.Show
    Else
        MsgBox "Invalid Password, try again!", , "Login"
        txtPassword.SetFocus
        SendKeys "{Home}+{End}"
    End If
End Sub

Private Sub Form_Load()

' ========== LOAD SKIN ==============
Dim Skinpath As String
Skinpath = App.Path & "\LinuxGnome.skn"
Skn.LoadSkin Skinpath
Skn.ApplySkin Me.hwnd

End Sub

