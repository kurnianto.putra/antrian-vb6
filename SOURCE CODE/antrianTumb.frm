VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form antrianPanel 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   Caption         =   ".:SUPPORT BY IT TABRANI ANDALAN BERSAMA:."
   ClientHeight    =   1770
   ClientLeft      =   6975
   ClientTop       =   3795
   ClientWidth     =   5865
   Icon            =   "antrianTumb.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1770
   ScaleWidth      =   5865
   Begin ACTIVESKINLibCtl.SkinLabel lblnextqueue 
      Height          =   495
      Left            =   3360
      OleObjectBlob   =   "antrianTumb.frx":038A
      TabIndex        =   13
      Top             =   600
      Width           =   615
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
      Height          =   495
      Left            =   2760
      OleObjectBlob   =   "antrianTumb.frx":0404
      TabIndex        =   12
      Top             =   600
      Width           =   615
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   495
      Left            =   2520
      OleObjectBlob   =   "antrianTumb.frx":0466
      TabIndex        =   11
      Top             =   120
      Width           =   1935
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   495
      Left            =   120
      OleObjectBlob   =   "antrianTumb.frx":04E0
      TabIndex        =   10
      Top             =   120
      Width           =   1935
   End
   Begin ACTIVESKINLibCtl.SkinLabel lblmyqueue 
      Height          =   495
      Left            =   960
      OleObjectBlob   =   "antrianTumb.frx":055A
      TabIndex        =   9
      Top             =   600
      Width           =   615
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
      Height          =   495
      Left            =   240
      OleObjectBlob   =   "antrianTumb.frx":05B8
      TabIndex        =   8
      Top             =   600
      Width           =   735
   End
   Begin ACTIVESKINLibCtl.Skin skn 
      Left            =   3360
      OleObjectBlob   =   "antrianTumb.frx":061A
      Top             =   2040
   End
   Begin VB.TextBox txtjenis 
      Height          =   495
      Left            =   0
      TabIndex        =   6
      Text            =   "BPJS"
      Top             =   2520
      Width           =   2535
   End
   Begin VB.CommandButton cmdReset 
      Appearance      =   0  'Flat
      Caption         =   "setting"
      Height          =   375
      Left            =   5040
      TabIndex        =   5
      Top             =   720
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.CommandButton cmdRecall 
      DownPicture     =   "antrianTumb.frx":1867F
      Enabled         =   0   'False
      Height          =   495
      Left            =   5040
      Picture         =   "antrianTumb.frx":18C09
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Re-Call"
      Top             =   1200
      Width           =   735
   End
   Begin VB.Timer timer_auto_refresh_next 
      Interval        =   100
      Left            =   6000
      Top             =   2760
   End
   Begin VB.TextBox txtLoket 
      Height          =   495
      Left            =   0
      TabIndex        =   3
      Text            =   "4"
      Top             =   1920
      Width           =   2535
   End
   Begin VB.CommandButton cmdSkip 
      Caption         =   "Skip"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   2
      Top             =   1200
      Width           =   1455
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "Next"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3600
      TabIndex        =   1
      Top             =   1200
      Width           =   1335
   End
   Begin VB.CommandButton cmdConfirm 
      Caption         =   "Confirm"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1680
      TabIndex        =   0
      Top             =   1200
      Width           =   1815
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Label1"
      Height          =   435
      Left            =   2160
      TabIndex        =   7
      Top             =   3360
      Width           =   1320
   End
End
Attribute VB_Name = "antrianPanel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim WithEvents RSList As Recordset
Attribute RSList.VB_VarHelpID = -1
Dim WithEvents FormSys As FrmSysTray
Attribute FormSys.VB_VarHelpID = -1
Dim lastqueue As String

Private Sub cmdConfirm_Click()
Call ActConn
Set RSList = New Recordset
RSList.Open "SELECT id FROM list WHERE status='ON' AND actions='Skip' AND no_urut='" + lblnextqueue.Caption + "'", strConn

If RSList.EOF Then
    SQLUpdate = "UPDATE list SET status='OFF', sound='0', actions='Proses' WHERE no_urut='" & lblnextqueue.Caption & "'"
    dbconn.Execute SQLUpdate
    cmdConfirm.Enabled = False
    cmdSkip.Enabled = False
    cmdRecall.Enabled = False
    
    lblmyqueue.Caption = lblnextqueue.Caption
    lblnextqueue.Caption = "?"
Else
    MsgBox "Status Nomor Antrian Anda Canceled", vbOKOnly, "critical"
    SQLUpdate = "UPDATE list SET status='OFF', sound='0' WHERE no_urut='" & lblnextqueue.Caption & "'"
    dbconn.Execute SQLUpdate
    
    cmdConfirm.Enabled = False
    cmdSkip.Enabled = False
    cmdRecall.Enabled = False
    
    lblnextqueue.Caption = "?"
End If



End Sub

Private Sub cmdNext_Click()
Call ActConn
Dim available As Integer
Dim jenis_antrian As String

Set RSList = New Recordset
jenis_antrian = txtjenis.Text
RSList.Open "SELECT MAX(no_urut) FROM list WHERE actions != 'Waiting' AND sound='0' AND status='OFF' AND jenis='" & jenis_antrian & "'", strConn

'jika data tidak ada
If IsNull(RSList(0)) Then
    available = "1"
    SQLInsert = "INSERT INTO list VALUES('" & txtLoket.Text & "','" & available & "','ON','1','Waiting',getdate(),'" & txtjenis.Text & "')"
    dbconn.Execute SQLInsert
    lblnextqueue.Caption = available
Else
    available = RSList(0)
    available = available + 1
    
    'if data more than 600
    If available > 600 Then
        MsgBox "Sudah Lebih Dari 600 Antrian, System Akan Mengulang dari 1", vbCritical
        'empty database
        SQLExce = "INSERT INTO list_history (no_urut,comp,status,sound,actions,dates,jenis) SELECT no_urut,comp,status,sound,actions,dates,jenis FROM list"
        SQLExce2 = "DELETE FROM list"
        
        dbconn.Execute SQLExce
        dbconn.Execute SQLExce2
        End
    End If
    
    SQLInsert = "INSERT INTO list VALUES('" & txtLoket.Text & "','" & available & "','ON','1','Waiting',getdate(),'" & txtjenis.Text & "' )"
    dbconn.Execute SQLInsert
    lblnextqueue.Caption = available
End If

cmdConfirm.Enabled = True
cmdConfirm.SetFocus
cmdSkip.Enabled = True
cmdNext.Enabled = False
cmdRecall.Enabled = True

End Sub

Private Sub cmdRecall_Click()
Call ActConn
SQLUpdate = "UPDATE list SET sound='1' WHERE no_urut='" & lblnextqueue.Caption & "'"
    dbconn.Execute SQLUpdate
End Sub

Private Sub cmdReset_Click()
    Frmmanage.Visible = True
    Me.Visible = False
End Sub

Private Sub cmdSkip_Click()
Call ActConn
SQLUpdate = "UPDATE list SET status='OFF', sound='0', actions='Skip' WHERE no_urut='" & lblnextqueue.Caption & "'"
dbconn.Execute SQLUpdate
lblnextqueue.Caption = "?"
cmdConfirm.Enabled = False
cmdRecall.Enabled = False
cmdSkip.Enabled = False
End Sub

Private Sub Form_Load()
Me.Left = (Screen.Width - Me.Width)
Me.Top = (Screen.Height - Me.Height * 2)
' ========== LOAD SKIN ==============
Dim Skinpath As String
Skinpath = App.Path & "\LinuxGnome.skn"
Skn.LoadSkin Skinpath
Skn.ApplySkin Me.hwnd
    
' Load FormSys; set reference to me
   Set FormSys = New FrmSysTray
   Load FormSys
   Set FormSys.FSys = Me
  
Call ActConn

status_login = Split(frmLogin.txtUserName.Text, " ")

Set RSList = New Recordset
RSList.Open "SELECT TOP 1(no_urut) FROM list WHERE status='OFF' AND actions!='Confirm' AND comp='" & status_login(1) & "' order by id DESC", strConn
    
'jika data baru tidak ada
If RSList.EOF Then
    lblmyqueue.Caption = "-"
Else
    lastqueue = RSList(0)
    lblmyqueue.Caption = RSList(0)
End If

'Jika memiliki antrian yg tertunda
Set RSget2 = New Recordset
RSget2.Open "SELECT TOP 1(no_urut) FROM list WHERE status='ON' AND actions='Waiting' AND comp='" & status_login(1) & "' order by id DESC", strConn
'jika data baru tidak ada
If RSget2.EOF Then
    lblnextqueue.Caption = "?"
Else
    MsgBox "Anda Memiliki 1 Antrian Yang Tertunda", vbOKOnly
    suspendqueue = RSget2(0)
    lblnextqueue.Caption = suspendqueue
    cmdRecall.Enabled = True
End If

If lblnextqueue.Caption = "?" Then
    cmdConfirm.Enabled = False
    cmdSkip.Enabled = False
End If


'Button setting
If status_login(1) = "1" Then
    cmdReset.Visible = True
End If
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
If ChkCloseMeansClose = 0 Then FrmSysTray.MeQueryUnload Me, Cancel, UnloadMode
End Sub


Private Sub SkinLabel5_DragDrop(Source As Control, x As Single, y As Single)

End Sub

Private Sub lblnextqueue_Click()

End Sub

Private Sub timer_auto_refresh_next_Timer()
Call ActConn

Set RSList = New Recordset
RSList.Open "SELECT no_urut FROM list WHERE status='ON' AND actions='Waiting'", strConn
  
'jika data baru tidak ada
If RSList.EOF Then
    cmdNext.Enabled = True
Else
    cmdNext.Enabled = False
End If

End Sub

