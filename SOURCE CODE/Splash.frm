VERSION 5.00
Begin VB.Form Splash 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   6150
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   8760
   LinkTopic       =   "Form1"
   Picture         =   "Splash.frx":0000
   ScaleHeight     =   6150
   ScaleWidth      =   8760
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   105
      Left            =   0
      Picture         =   "Splash.frx":37DDE
      ScaleHeight     =   75
      ScaleWidth      =   225
      TabIndex        =   1
      Top             =   4920
      Width           =   255
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   105
      Left            =   240
      Picture         =   "Splash.frx":3AF9E
      ScaleHeight     =   75
      ScaleWidth      =   8505
      TabIndex        =   0
      Top             =   4920
      Width           =   8535
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   70
      Left            =   1680
      Top             =   2280
   End
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   600
      Top             =   2280
   End
End
Attribute VB_Name = "Splash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" _
   (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" _
   (ByVal hwnd As Long, ByVal nIndex As Long, _
   ByVal dwNewLong As Long) As Long
Private Const GWL_STYLE = (-16)
Private Const GWL_EXSTYLE = (-20)
Private Declare Function SetLayeredWindowAttributes Lib "user32" _
   (ByVal hwnd As Long, ByVal crKey As Long, _
   ByVal bAlpha As Byte, ByVal dwFlags As Long) As Long
Private Const LWA_COLORKEY = &H1
Private Const LWA_ALPHA = &H2
Private Const WS_EX_LAYERED = &H80000

Private m_lAlpha As Long

Private Sub Form_Load()
   m_lAlpha = 255
End Sub



Private Sub Timer1_Timer()
Picture1.Visible = True
Picture1.Width = Picture1.Width + 200
If Picture1.Width >= Picture2.Width Then
    Timer1.Enabled = False
    Timer2.Enabled = True
    
End If
End Sub

Private Sub Timer2_Timer()

   If (m_lAlpha = 255) Then
      Dim lStyle As Long
      lStyle = GetWindowLong(Me.hwnd, GWL_EXSTYLE)
      lStyle = lStyle Or WS_EX_LAYERED
      SetWindowLong Me.hwnd, GWL_EXSTYLE, lStyle
   End If
   Dim lAlpha
   m_lAlpha = m_lAlpha - 50
   lAlpha = m_lAlpha
   If (lAlpha < 0) Then
      lAlpha = 0
   End If
   SetLayeredWindowAttributes Me.hwnd, 0, lAlpha, LWA_ALPHA
   If (m_lAlpha < 0) Then
      Unload Me
    frmLogin.Show
   End If
   
   
End Sub




