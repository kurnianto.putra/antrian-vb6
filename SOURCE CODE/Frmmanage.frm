VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form Frmmanage 
   BackColor       =   &H8000000B&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Setting"
   ClientHeight    =   4425
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7860
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4425
   ScaleWidth      =   7860
   StartUpPosition =   2  'CenterScreen
   Begin ACTIVESKINLibCtl.Skin Skn 
      Left            =   3600
      OleObjectBlob   =   "Frmmanage.frx":0000
      Top             =   3840
   End
   Begin VB.CommandButton btnSkip4 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   6240
      TabIndex        =   23
      Top             =   2640
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox Text3 
      Height          =   495
      Left            =   9960
      TabIndex        =   12
      Text            =   "Text3"
      Top             =   1920
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text2 
      Height          =   495
      Left            =   9960
      TabIndex        =   11
      Text            =   "Text2"
      Top             =   1080
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text1 
      Height          =   495
      Left            =   9960
      TabIndex        =   10
      Text            =   "Text1"
      Top             =   240
      Visible         =   0   'False
      Width           =   1350
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "RESET"
      Height          =   375
      Left            =   240
      TabIndex        =   9
      Top             =   3360
      Width           =   5895
   End
   Begin VB.CommandButton btnSkip3 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   6240
      TabIndex        =   8
      Top             =   2040
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton btnSkip2 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   6240
      TabIndex        =   7
      Top             =   1200
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton btnSkip1 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   6240
      TabIndex        =   6
      Top             =   360
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Label9"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5400
      TabIndex        =   22
      Top             =   2640
      Width           =   975
   End
   Begin VB.Label Label8 
      Caption         =   "No Antri"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4320
      TabIndex        =   21
      Top             =   2640
      Width           =   1095
   End
   Begin VB.Label LbInfo4 
      BackColor       =   &H0000FFFF&
      Height          =   375
      Left            =   2640
      TabIndex        =   20
      Top             =   2640
      Width           =   1575
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Informasi 4   :"
      BeginProperty Font 
         Name            =   "Courier"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   19
      Top             =   2640
      Width           =   2655
   End
   Begin VB.Label Label6 
      Caption         =   "No. Antri"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4320
      TabIndex        =   18
      Top             =   2040
      Width           =   975
   End
   Begin VB.Label Label5 
      Caption         =   "No. Antri"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4320
      TabIndex        =   17
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "No. Antri"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4320
      TabIndex        =   16
      Top             =   360
      Width           =   975
   End
   Begin VB.Label lbl3 
      BackStyle       =   0  'Transparent
      Caption         =   "Label6"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   15
      Top             =   2040
      Width           =   735
   End
   Begin VB.Label lbl2 
      BackStyle       =   0  'Transparent
      Caption         =   "Label5"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   14
      Top             =   1200
      Width           =   735
   End
   Begin VB.Label lbl1 
      BackStyle       =   0  'Transparent
      Caption         =   "Label4"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   13
      Top             =   360
      Width           =   735
   End
   Begin VB.Label lblInfo3 
      Alignment       =   2  'Center
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   5
      Top             =   2040
      Width           =   1575
   End
   Begin VB.Label lblInfo2 
      Alignment       =   2  'Center
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   4
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label lblInfo1 
      Alignment       =   2  'Center
      BackColor       =   &H0000FF00&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   3
      Top             =   360
      Width           =   1575
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Informasi 3   : "
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   2040
      Width           =   2415
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Informasi 2   : "
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   1200
      Width           =   2415
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Informasi 1   : "
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   360
      Width           =   2415
   End
End
Attribute VB_Name = "Frmmanage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim WithEvents RSList As Recordset
Attribute RSList.VB_VarHelpID = -1

Private Sub btnSkip1_Click()
Dim response
response = MsgBox("Yakin Untuk Cancel Proses ?", vbYesNo + vbQuestion, "critical")

If response = vbYes Then
SQLDelete = "Update list set actions='Skip' Where id= '" + Text1.Text + "'"
    dbconn.Execute SQLDelete
lblInfo1.BackColor = vbRed
lblInfo1.Caption = "Skip"
End If
End Sub

Private Sub btnSkip2_Click()
Dim response
response = MsgBox("Yakin Untuk Cancel Proses ?", vbYesNo + vbQuestion, "critical")

If response = vbYes Then
SQLDelete = "Update list set actions='Skip' Where id= '" + Text2.Text + "'"
    dbconn.Execute SQLDelete
lblInfo2.BackColor = vbRed
lblInfo2.Caption = "Skip"
End If
End Sub

Private Sub btnSkip3_Click()
Dim response
response = MsgBox("Yakin Untuk Cancel Proses ?", vbYesNo + vbQuestion, "critical")

If response = vbYes Then
SQLDelete = "Update list set actions='Skip' Where id= '" + Text3.Text + "'"
    dbconn.Execute SQLDelete
lblInfo3.BackColor = vbRed
lblInfo3.Caption = "Skip"
End If
End Sub

Private Sub btnSkip4_Click()
Dim response
response = MsgBox("Yakin Untuk Cancel Proses ?", vbYesNo + vbQuestion, "critical")

If response = vbYes Then
SQLDelete = "Update list set actions='Skip' Where id= '" + Text4.Text + "'"
    dbconn.Execute SQLDelete
lblInfo4.BackColor = vbRed
lblInfo4.Caption = "Skip"
End If
End Sub

Private Sub cmdReset_Click()
Dim response
response = MsgBox("Yakin Untuk Menghapus Data ?", vbYesNo + vbQuestion, "critical")

If response = vbYes Then
SQLDelete = "Delete From list"
    dbconn.Execute SQLDelete
 
MsgBox "Success Reset, Please Close All Client Program"

Unload Me
End If
End Sub

Private Sub Form_Load()
Call status


End Sub

Private Sub Form_Unload(Cancel As Integer)

    antrianPanel.Visible = True
    Unload Me
    

End Sub

Private Sub status()
Call ActConn

'get informasi 1
Set RSList = New Recordset
RSList.Open "select top 1 id, no_urut, comp, actions from list where comp='1' order by id desc", strConn

'jika data tidak ada
If RSList.EOF Then
    lblInfo1.Caption = "-"
    lbl1.Caption = "-"
Else
    If (RSList(3) = "Waiting") Then
        lblInfo1.Caption = RSList(3) + "..."
        lblInfo1.BackColor = vbGreen
        btnSkip1.Visible = True
        Text1.Text = RSList(0)
    ElseIf (RSList(3) = "Skip") Then
        lblInfo1.Caption = RSList(3)
        lblInfo1.BackColor = vbRed
    Else
        lblInfo1.Caption = RSList(3)
        lblInfo1.BackColor = vbActiveBorder
    End If
    lbl1.Caption = RSList(1)
End If

'get informasi 2
Set RSList = New Recordset
RSList.Open "select top 1 id, no_urut, comp, actions from list where comp='2' order by id desc", strConn

'jika data tidak ada
If RSList.EOF Then
    lblInfo2.Caption = "-"
    lbl2.Caption = "-"
Else
    If (RSList(3) = "Waiting") Then
        lblInfo2.Caption = RSList(3) + "..."
        lblInfo2.BackColor = vbGreen
        btnSkip2.Visible = True
        Text2.Text = RSList(0)
    ElseIf (RSList(3) = "Skip") Then
        lblInfo2.Caption = RSList(3)
        lblInfo2.BackColor = vbRed
    Else
        lblInfo2.Caption = RSList(3)
        lblInfo2.BackColor = vbActiveBorder
    End If
    lbl2.Caption = RSList(1)
End If

'get informasi 3
Set RSList = New Recordset
RSList.Open "select top 1 id, no_urut, comp, actions from list where comp='3' order by id desc", strConn

'jika data tidak ada
If RSList.EOF Then
    lblInfo3.Caption = "-"
    lbl3.Caption = "-"
Else
    If (RSList(3) = "Waiting") Then
        lblInfo3.Caption = RSList(3) + "..."
        lblInfo3.BackColor = vbGreen
        btnSkip3.Visible = True
        Text3.Text = RSList(0)
    ElseIf (RSList(3) = "Skip") Then
        lblInfo3.Caption = RSList(3)
        lblInfo3.BackColor = vbRed
    Else
        lblInfo3.Caption = RSList(3)
        lblInfo3.BackColor = vbActiveBorder
    End If
    lbl3.Caption = RSList(1)
End If

End Sub

